<?php
/**
 * Created by PhpStorm.
 * User: semihs
 * Date: 15.07.2016
 * Time: 20:31
 */

require_once __DIR__ . '/../lib/Service/CheckDuplicateItemService.php';
?>

<?php
$itemList = array(
    array('A', 'A', 'A', 'D', 'G'),
    array('C', 'A', 'A', 'A', 'E'),
    array('B', 'B', 'B', 'E', 'B'),
    array('A', 'A', 'B', 'A', 'A'),
    array('A', 'J', 'A', 'C', 'E'),
    array('J', 'A', 'A', 'A', 'G'),
    array('J', 'J', 'A', 'J', 'G'),
    array('A', 'A', 'A', 'D', 'J')
);
if (php_sapi_name() == 'cli') {
    foreach ($itemList as $items) {
        $checkDuplicateItemService = new CheckDuplicateItemService($items);
        print_r($checkDuplicateItemService->getResponse());
    }
    ?>
<?php } else { ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Beyn Interview Question</title>
    </head>
    <body>
    Test Form
    <form method="post" action="">
        <?php
        if (!empty($_POST['items'])) {
            $items = array_filter($_POST['items'], 'strip_tags');
        }
        ?>
        <?php for ($x = 1; $x <= 5; $x++) { ?>
            <input type="text" name="items[]" placeholder="char <?php echo $x; ?>"
                <?php if (!empty($items[$x - 1])) { ?>
                    value="<?php echo $items[$x - 1]; ?>"
                <?php } ?>/>
        <?php } ?>
        <input type="submit" name="submit" value="Submit"/>
    </form>
    <?php if (!empty($_POST)) { ?>
        <?php
        $checkDuplicateItemService = new CheckDuplicateItemService($items);
        $response = $checkDuplicateItemService->getResponse();
        ?>
        Test Form Results
        <table border="1" cellpadding="5" style="width: 500px;">
            <tr>
                <th>Dizi Elemanlari</th>
                <th>Status</th>
                <th>Karakter</th>
                <th>Adet</th>
            </tr>
            <tr>
                <td><?php echo implode(',', $items); ?></td>
                <td><?php echo $response->isStatus() ? 'true' : 'false'; ?></td>
                <td><?php echo $response->getSymbol(); ?></td>
                <td><?php echo $response->getCount(); ?></td>
            </tr>
        </table>

    <?php } ?>
    Examples
    <table border="1" cellpadding="5" style="width: 500px;">
        <tr>
            <th>Dizi Elemanlari</th>
            <th>Status</th>
            <th>Karakter</th>
            <th>Adet</th>
        </tr>
        <?php foreach ($itemList as $items) { ?>
            <?php
            $checkDuplicateItemService = new CheckDuplicateItemService($items);
            $response = $checkDuplicateItemService->getResponse();
            ?>
            <tr>
                <td><?php echo implode(',', $items); ?></td>
                <td><?php echo $response->isStatus() ? 'true' : 'false'; ?></td>
                <td><?php echo $response->getSymbol(); ?></td>
                <td><?php echo $response->getCount(); ?></td>
            </tr>
        <?php } ?>
    </table>
    </body>
    </html>
    <?php
}
?>