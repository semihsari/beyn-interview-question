<?php

require __DIR__ . '/../../lib/Model/Response.php';

/**
 * User: semihs
 * Date: 15.07.2016
 * Time: 20:32
 */
class CheckDuplicateItemService
{
    protected $_duplicate_count = 0;
    protected $_joker_count = 0;
    protected $_valid_item_found = false;
    protected $_searching_item;
    protected $_prev_item;
    protected $_response;

    public function __construct(array $items)
    {
        foreach ($items as $item) {
            if ($item == 'J') {
                $this->incJokerCount();

                if ($this->isValidItemFound() === false) {
                    $this->setPrevItem($item);
                    continue;
                }
            }
            $this->setValidItemFound(true);

            if (empty($this->getSearchingItem())) {
                $this->setSearchingItem($item);
                if ($item != 'J') {
                    $this->incDuplicateCount();
                }
            } else if ($item != $this->getPrevItem() && $item != 'J' && $this->getPrevItem() != 'J') {
                $this->setPrevItem($item);
                break;
            } else if ($item == $this->getSearchingItem() && $item == $this->getPrevItem() && $item != 'J') {
                $this->incDuplicateCount();
            } else if ($item == $this->getSearchingItem() && $this->getPrevItem() == 'J' && $item != 'J') {
                $this->incDuplicateCount();
            }
            $this->setPrevItem($item);
        }

        $this->incDuplicateCount($this->getJokerCount());

        if ($this->getDuplicateCount() >= 3) {
            $status = true;
        } else {
            $status = false;
        }

        $this->setResponse(new Response(array(
            'status' => $status,
            'symbol' => $this->getSearchingItem(),
            'count' => $this->getDuplicateCount(),
        )));
    }

    /**
     * @param int $inc
     *
     * @return void
     */
    public function incDuplicateCount($inc = 1)
    {
        $this->_duplicate_count = $this->_duplicate_count + $inc;
    }

    /**
     * @return int
     */
    public function getDuplicateCount()
    {
        return $this->_duplicate_count;
    }

    /**
     * @param int $duplicate_count
     */
    public function setDuplicateCount($duplicate_count)
    {
        $this->_duplicate_count = $duplicate_count;
    }

    /**
     * @return boolean
     */
    public function isValidItemFound()
    {
        return $this->_valid_item_found;
    }

    /**
     * @param boolean $valid_item_found
     */
    public function setValidItemFound($valid_item_found)
    {
        $this->_valid_item_found = $valid_item_found;
    }

    /**
     * @return string
     */
    public function getSearchingItem()
    {
        return $this->_searching_item;
    }

    /**
     * @param string $searching_item
     */
    public function setSearchingItem($searching_item)
    {
        $this->_searching_item = $searching_item;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->_response;
    }

    /**
     * @param Response $response
     */
    public function setResponse($response)
    {
        $this->_response = $response;
    }

    /**
     * @return void
     */
    public function incJokerCount()
    {
        $this->_joker_count++;
    }

    /**
     * @return int
     */
    public function getJokerCount()
    {
        return $this->_joker_count;
    }

    /**
     * @param int $joker_count
     */
    public function setJokerCount($joker_count)
    {
        $this->_joker_count = $joker_count;
    }

    /**
     * @return mixed
     */
    public function getPrevItem()
    {
        return $this->_prev_item;
    }

    /**
     * @param mixed $prev_item
     */
    public function setPrevItem($prev_item)
    {
        $this->_prev_item = $prev_item;
    }
}