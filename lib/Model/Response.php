<?php

/**
 * User: semihs
 * Date: 15.07.2016
 * Time: 20:31
 */
class Response
{
    /**
     * @var boolean
     */
    protected $_status;
    /**
     * @var string
     */
    protected $_symbol;
    /**
     * @var int
     */
    protected $_count;

    public function __construct(array $data)
    {
        foreach($data as $key => $value) {
            $property = '_' . $key;
            if (property_exists($this, $property)) {
                $this->{$property} = $value;
            } else {
                throw new Exception('Unknown Response Model Option: ' . $property);
            }
        }
    }

    /**
     * @return boolean
     */
    public function isStatus()
    {
        return $this->_status;
    }

    /**
     * @param boolean $status
     */
    public function setStatus($status)
    {
        $this->_status = $status;
    }

    /**
     * @return string
     */
    public function getSymbol()
    {
        return $this->_symbol;
    }

    /**
     * @param string $symbol
     */
    public function setSymbol($symbol)
    {
        $this->_symbol = $symbol;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->_count;
    }

    /**
     * @param int $count
     */
    public function setCount($count)
    {
        $this->_count = $count;
    }
}